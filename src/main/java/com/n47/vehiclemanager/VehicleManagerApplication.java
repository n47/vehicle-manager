package com.n47.vehiclemanager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class VehicleManagerApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(VehicleManagerApplication.class, args);
		} catch (Exception e) {
			System.out.println("============================");
			System.out.println(e);
		}
	}

}
