package com.n47.vehiclemanager.model;

public enum Currency {
    EUR,
    USD,
    CHF,
    MKD
}
