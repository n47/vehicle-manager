package com.n47.vehiclemanager.model;

public enum VehicleType {
    MOTORBIKE,
    CAR,
    VAN,
    BUS,
    TRUCK
}
