package com.n47.vehiclemanager.service;

import com.n47.vehiclemanager.model.Currency;
import com.n47.vehiclemanager.model.Vehicle;
import com.n47.vehiclemanager.model.VehicleType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleService {

    List<Vehicle> vehicles = new ArrayList<>();

    {
        vehicles.add(Vehicle.builder()
                        .vehicleType(VehicleType.CAR)
                        .registrationPlate("KO1234AB")
                        .seatsCount(5)
                        .price(22_000)
                        .currency(Currency.EUR)
                        .available(true)
                        .build());
        vehicles.add(Vehicle.builder()
                        .vehicleType(VehicleType.BUS)
                        .registrationPlate("SK1234AB")
                        .seatsCount(51)
                        .price(120_000)
                        .currency(Currency.EUR)
                        .available(true)
                        .build());
    }

    public void addVehicle(Vehicle vehicle) {
        this.vehicles.add(vehicle);
    }

    public Vehicle getVehicle(String registrationPlate) throws Exception {
        for (Vehicle vehicle: vehicles) {
            if (vehicle.getRegistrationPlate().equals(registrationPlate)) {
                return vehicle;
            }
        }
        throw new Exception("Vehicle not found!");
    }
}
